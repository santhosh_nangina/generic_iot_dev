/* 
 * File:   i2c.h
 * Author: jlanka
 *
 * Created on 19 June, 2014, 3:23 PM
 */

#ifndef MC_I2C_H
#define	MC_I2C_H

#ifdef	__cplusplus
extern "C" {
#endif

#define PORT_1 1
#define PORT_2 2

int Init_I2C(int port);

int i2c_write8 (int port,
        unsigned char dev_addr,
        unsigned char cmd,
        unsigned char data);
int i2c_read8 (int port,
        unsigned char dev_addr,
        unsigned char cmd,
        unsigned char *data);
int i2c_read_multiple (int port,
        unsigned char dev_addr,
        unsigned char *data,
        unsigned int size);
int i2c_write_multiple (int port,
        unsigned char dev_addr,
        unsigned char *data,
        unsigned int size);


#ifdef	__cplusplus
}
#endif

#endif	/* I2C_H */

