/* 
 * File:   tf_i2c.h
 * Author: abhishek
 *
 * Created on 22 February, 2016, 3:02 PM
 */

#ifndef TF_I2C_H
#define	TF_I2C_H

#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif
    /**
     * Initializes I2C module.
     * @param port I2C port number.
     */
    void tf_i2c_init(unsigned char port);
    /**
     * Single byte read over I2C.
     * @param port I2C port number.
     * @param dev_addr I2C device address.
     * @param reg_addr Register address.
     * @param data Pointer to data buffer.
     * @return 0 if successful, -1 if failed.
     */
    char tf_i2c_read8(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr,
                    unsigned char *data);
    /**
     * Single byte write to I2C.
     * @param port I2C port number.
     * @param dev_addr I2C device address.
     * @param reg_addr Register address.
     * @param data Data to be written.
     * @return 0 if successful, -1 if failed.
     */
    char tf_i2c_write8(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr,
                    unsigned char data);
    /**
     * Single byte I2C read when the register address is two bytes.
     * @param port I2C port number.
     * @param dev_addr I2C device address.
     * @param reg_addr_upper Upper byte of register address.
     * @param reg_addr_lower Lower byte of register address.
     * @param data Pointer to data buffer.
     * @return 0 if successful, -1 if failed.
     */
    char tf_i2c_read8_2byte_reg_addr(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr_upper,
                    unsigned char reg_addr_lower,
                    unsigned char *data);
    /**
     * Reads two bytes over I2C.
     * @param port I2C port number.
     * @param dev_addr I2C device address.
     * @param reg_addr Register address.
     * @param data Pointer to data buffer.
     * @return 0 if successful, -1 if failed.
     */
    uint16_t tf_i2c_read16(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr,
                    unsigned short *data);
    /**
     * Single byte write to a register with two byte address.
     * @param port I2C port number.
     * @param dev_addr I2C device address.
     * @param reg_addr_upper Upper byte of register address.
     * @param reg_addr_lower Lower byte of register address.
     * @param data Data to be written.
     * @return 0 if successful, -1 if failed.
     */
    char tf_i2c_write8_2byte_reg_addr(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr_upper,
                    unsigned char reg_addr_lower,
                    unsigned char data);


#ifdef	__cplusplus
}
#endif

#endif	/* TF_I2C_H */

