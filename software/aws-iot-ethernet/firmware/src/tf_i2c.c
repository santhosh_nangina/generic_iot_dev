
#include <plib.h>

//#include "sys_config.h"
#include "tf_i2c.h"
#define I2C_400KHZ 0

#if (I2C_400KHZ)
#define FOSC        (400000L)
#define I2C_SLEW    I2C_SLW_EN
#else
#define FOSC (100000L)
#define I2C_SLEW    I2C_SLW_DIS
#endif

#define SYS_CLK 80000000L 
#define PB_CLK (SYS_CLK / 1)


unsigned char current_MSB, current_LSB;

void tf_i2c_init(unsigned char port) {
    if (port == 1) {
        unsigned int config = I2C_ON | I2C_IDLE_STOP | I2C_SLEW | I2C_ACK |
                                I2C_ACK_EN | I2C_RCV_EN | I2C_STOP_EN |
                                I2C_RESTART_EN | I2C_START_EN;
        // BRG = (Fpb / 2 / baudrate) - 2
        unsigned int brg = (PB_CLK / 2 / FOSC) - 2;
        OpenI2C1(config, brg);
    }
    // Currently only port 2 is used    
    if (port == 2) {
        unsigned int config = I2C_ON | I2C_IDLE_STOP | I2C_SLEW | I2C_ACK |
                                I2C_ACK_EN | I2C_RCV_EN | I2C_STOP_EN |
                                I2C_RESTART_EN | I2C_START_EN;
        // BRG = (Fpb / 2 / baudrate) - 2
        unsigned int brg = (PB_CLK / 2 / FOSC) - 2;
        OpenI2C2(config, brg);
    }    
}

uint8_t data1_temp, data2_temp;
uint16_t returnData;

uint16_t tf_i2c_read16(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr,
                    unsigned short *data){
    
    if(port ==1){
        StartI2C1();
        IdleI2C1();
        MasterWriteI2C1((dev_addr << 1) | 0x01);
        IdleI2C1();
        data1_temp  = MasterReadI2C1();
        IdleI2C1();
        AckI2C1();
        IdleI2C1();
        data2_temp = MasterReadI2C1();
        IdleI2C1();
        NotAckI2C1();
        IdleI2C1();
        StopI2C1();
        returnData = 0x0000;
        returnData = returnData | data1_temp;
        returnData  = (returnData << 8);
        returnData = returnData | data2_temp ;
        return returnData ;
    }
}
    
char tf_i2c_read8(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr,
                    unsigned char *data) {
    // Currently only port 2 is used
    if (port == 1) {
        StartI2C1();
        IdleI2C1();
        MasterWriteI2C1((dev_addr << 1) | 0x01);
        IdleI2C1();
        current_MSB = MasterReadI2C1();
        IdleI2C1();
        AckI2C1();
        IdleI2C1();
        current_LSB = MasterReadI2C1();
        IdleI2C1();
        NotAckI2C1();
        
        IdleI2C1();
        StopI2C1();
        return 0;
    }
    // Currently only port 2 is used
    if (port == 2) {
        StartI2C2();
        IdleI2C2();
        MasterWriteI2C2((dev_addr << 1) & 0xFE);
        IdleI2C2();
        MasterWriteI2C2(reg_addr);
        IdleI2C2();

        RestartI2C2();
        IdleI2C2();
        MasterWriteI2C2((dev_addr << 1) | 0x01);
        IdleI2C2();
        *data = MasterReadI2C2();
        IdleI2C2();
        NotAckI2C2();
        IdleI2C2();
        StopI2C2();
        return 0;
    }
    return -1;
}

char tf_i2c_read8_2byte_reg_addr(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr_upper,
                    unsigned char reg_addr_lower,
                    unsigned char *data) {
    // Currently only port 2 is used
    if (port == 2) {
        StartI2C2();
        IdleI2C2();
        MasterWriteI2C2((dev_addr << 1) & 0xFE);
        IdleI2C2();
        MasterWriteI2C2(reg_addr_upper);
        IdleI2C2();
        MasterWriteI2C2(reg_addr_lower);
        IdleI2C2();

        RestartI2C2();
        IdleI2C2();
        MasterWriteI2C2((dev_addr << 1) | 0x01);
        IdleI2C2();
        *data = MasterReadI2C2();
        IdleI2C2();
        NotAckI2C2();
        IdleI2C2();
        StopI2C2();
        return 0;
    }
    return -1;
}

char tf_i2c_write8(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr,
                    unsigned char data) {
    // Currently only port 2 is used
    if (port == 2)
    {
        StartI2C2();
        IdleI2C2();
        MasterWriteI2C2((dev_addr << 1) & 0xFE);
        IdleI2C2();
        MasterWriteI2C2(reg_addr);
        IdleI2C2();
        MasterWriteI2C2(data);
        IdleI2C2();
        StopI2C2();
        return 0;
    }
    return -1;
}

char tf_i2c_write8_2byte_reg_addr(unsigned char port,
                    unsigned char dev_addr,
                    unsigned char reg_addr_upper,
                    unsigned char reg_addr_lower,
                    unsigned char data) {
    // Currently only port 2 is used
    if (port == 2)
    {
        StartI2C2();
        IdleI2C2();
        MasterWriteI2C2((dev_addr << 1) & 0xFE);
        IdleI2C2();
        MasterWriteI2C2(reg_addr_upper);
        IdleI2C2();
        MasterWriteI2C2(reg_addr_lower);
        IdleI2C2();
        MasterWriteI2C2(data);
        IdleI2C2();
        StopI2C2();
        return 0;
    }
    return -1;
}


