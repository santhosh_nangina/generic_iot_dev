/**
 * @file SHT21.c
 * @author Firmware department
 * @date 16 Mar 2016
 * @brief c File which contains every functions to use the temperature and humidity
 * sensor SHT21.
 * 
 * @see http://www.argotec.es
 */ 

#include "SHT21.h"
#include "TimeDelay.h"

// SHT21 structs info
SHT21_t SHT21Data_Inf;

/********************************************/

// Local I2C functions
void I2C_SHT21_Start(void);
void I2C_SHT21_Stop(void);
void I2C_SHT21_Restart(void);
void I2C_SHT21_Write(UINT8 value);
UINT16 I2C_SHT21_Read(void);
void I2C_SHT21_ACK(BOOL ack);

/********************************************/

void SHT21Init()
{
    // Initialize I2C0
    PLIB_I2C_BaudRateSet(I2C_ID_3, SYS_CLK_PeripheralFrequencyGet(CLK_BUS_PERIPHERAL_2), 50000);
    PLIB_I2C_StopInIdleDisable(I2C_ID_3);
    // Low frequency is enabled (**NOTE** PLIB function logic reverted)
    PLIB_I2C_HighFrequencyEnable(I2C_ID_3);
    // set SDA to 0
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_F, PORTS_BIT_POS_2);
    // set SCL to 1
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_F, PORTS_BIT_POS_8);
    // set SDA to O/P
    PLIB_PORTS_PinDirectionOutputSet(PORTS_ID_0, PORT_CHANNEL_F, PORTS_BIT_POS_2);
    // set SCL to I/P
    PLIB_PORTS_PinDirectionInputSet(PORTS_ID_0, PORT_CHANNEL_F, PORTS_BIT_POS_8);
    // Enable I2C0
    PLIB_I2C_Enable(I2C_ID_3);
    
    // Reset struct values: Temperature and humidity
    SHT21Data_Inf.Temperature = 0.0;
    SHT21Data_Inf.Humidity = 0.0;
}

float SHT21GetValue(UINT8 Temp_Hum, BOOL raw) 
{
    // Response variable
    UINT16 SensorResponse = 0;

    // Start
    I2C_SHT21_Start();
    // Send sensor's address  
    I2C_SHT21_Write(SHT21_Address);
    // Sensor commands
     switch (Temp_Hum) {
        case eTempHoldCmd:
            I2C_SHT21_Write(eTempHoldCmd);
            break;
        case eRHumidityHoldCmd:
            I2C_SHT21_Write(eRHumidityHoldCmd);
            break;
        case eTempNoHoldCmd:
        case eRHumidityNoHoldCmd:
        default:
            #ifdef DEBUG_INFO
                #error: There was an error with the command selected.
            #endif
            return 0.0;
    }
    // Delay needed by the datasheet
    DelayMs(100);
    // I2C bus restart
    I2C_SHT21_Restart();
     // Send sensor's address or 0x01
    I2C_SHT21_Write(SHT21_Address | 0x01);
    // Delay for security
    DelayMs(1);
    // Store the result
    SensorResponse = I2C_SHT21_Read();
    // Send ACK
    I2C_SHT21_ACK(TRUE);
    // Clear two low bits (status bits)
    SensorResponse &= ~0x0003;   // clear two low bits (status bits)
    // I2C stop bus
    I2C_SHT21_Stop();
    
    // Sensor values as raw or converted
    switch (Temp_Hum) {
        case eTempHoldCmd:
            if(raw == TRUE)
                SHT21Data_Inf.Temperature = (float)SensorResponse;
            else if(raw == FALSE)
                SHT21Data_Inf.Temperature = (-46.85 + 175.72 / 65536.0 * (float)(SensorResponse));
            else {
                #ifdef DEBUG_INFO
                    #error: There was an error taking the value, it must be raw or not.
                #endif
                SHT21Data_Inf.Temperature = 0.0;
            }
            return SHT21Data_Inf.Temperature;
        case eRHumidityHoldCmd:
            if(raw == TRUE)
                SHT21Data_Inf.Humidity = (float)SensorResponse;
            else if (raw == FALSE)
                SHT21Data_Inf.Humidity = (-6.0 + 125.0 / 65536.0 * (float)(SensorResponse));
            else {
                #ifdef DEBUG_INFO
                    #error: There was an error taking the value, it must be raw or not.
                #endif
                SHT21Data_Inf.Humidity = 0.0;
            }
            return SHT21Data_Inf.Humidity;
        case eTempNoHoldCmd:
        case eRHumidityNoHoldCmd:
        default:
            #ifdef DEBUG_INFO
                #error: There was an error with the command selected.
            #endif
            return 0.0;
    }
}

void I2C_SHT21_Start(void) 
{
    while (!DRV_I2C0_MasterBusIdle());
    DRV_I2C0_MasterStart();
    DRV_I2C0_WaitForStartComplete();
    while (!DRV_I2C0_MasterBusIdle());
}

void I2C_SHT21_Stop(void) 
{
    DRV_I2C0_MasterStop();
    DRV_I2C0_WaitForStopComplete();
    while (!DRV_I2C0_MasterBusIdle());
}

void I2C_SHT21_Restart(void) 
{
    DRV_I2C0_MasterRestart();
    DRV_I2C0_WaitForStartComplete();
    while (!DRV_I2C0_MasterBusIdle());
}

void I2C_SHT21_Write(UINT8 value)
{
    DRV_I2C0_ByteWrite(value);
    DRV_I2C0_WaitForByteWriteToComplete();
    while (!DRV_I2C0_MasterBusIdle());
}

UINT16 I2C_SHT21_Read(void)
{
    UINT16 valueRead = 0;
    DRV_I2C0_SetUpByteRead();
    while(!DRV_I2C0_WaitForReadByteAvailable());
    valueRead = DRV_I2C0_ByteRead() << 8;
    valueRead += DRV_I2C0_ByteRead();
    
    return valueRead;
}

void I2C_SHT21_ACK(BOOL ack) 
{
    if(ack == TRUE) {
        I2C3CONbits.ACKDT = 1;
        I2C3CONbits.ACKEN = 1;
    }
    else if (ack == FALSE) {
        I2C3CONbits.ACKDT = 0;
        I2C3CONbits.ACKEN = 1;
    }
    else 
        ;
}