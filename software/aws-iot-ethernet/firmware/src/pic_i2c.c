/*
 * J - 7/19/2014
 * I2C driver
 */

#include <xc.h>
#include <plib/i2c.h>
#include <device/pic_i2c.h>

#define SYS_FREQ (48000000L)

/*
 * I2C_100K_MODE:
 * 1 to enable 100KHz clock,
 * 0 to enable 400KHz clock
 */
#define I2C_100K_MODE 1

/* Defines for I2C clock */
/* In Master mode, I2C clock = FOSC/(4 * (SSPxADD + 1)), and,
** the Baud Rate Generator (BRG) reload value is to be placed in the
** lower 7 bits of the SSPxADD register i.e SSPxADD<6:0> */
#if I2C_100K_MODE
#define I2C_CLOCK_VALUE (100000L) /* 100 KHz */
#else
#define I2C_CLOCK_VALUE (400000L) /* 400 KHz */
#endif

#define I2C_BRG_VALUE (((SYS_FREQ / (4*I2C_CLOCK_VALUE)) - 1) & 0x7F)

#ifdef   __18F47J50
#define I2C1_SCL        TRISBbits.TRISB4
#define I2C1_SDA        TRISBbits.TRISB5

#define I2C2_SCL        TRISDbits.TRISD0
#define I2C2_SDA        TRISDbits.TRISD1
#endif
#ifdef   __18F47J53
#define I2C1_SCL        TRISBbits.TRISB4
#define I2C1_SDA        TRISBbits.TRISB5

#define I2C2_SCL        TRISDbits.TRISD0
#define I2C2_SDA        TRISDbits.TRISD1
#endif

// I2C recovery support constants
#define SDA_DIR     TRISDbits.TRISD1
#define SCK_DIR     TRISDbits.TRISD0
    
#define I2C_SPEED       100
#define HalfBitDelay    500/I2C_SPEED


#define Set_SDA_Low     SDA_DIR = 0
#define Set_SDA_High    SDA_DIR = 1
#define Set_SCK_Low     SCK_DIR = 0
#define Set_SCK_High    SCK_DIR = 1

static unsigned char transaction_in_flight = 0;
static unsigned char i2c_read_error = 0;
static unsigned char i2c_write_error = 0;

// Start i2c recovery code
static void InitI2C_recovery(void)
{
    Set_SDA_High;
    Set_SCK_High;
}

static void I2C_Start_recovery()
{
    Set_SCK_High;
    Set_SDA_High;
    Delay10KTCYx(HalfBitDelay);
    LATDbits.LATD0 = 0;
    LATDbits.LATD1 = 0;
    Set_SDA_Low;
    Delay10KTCYx(HalfBitDelay);
    
}

static void I2C_Stop_recovery(void)
{
    Set_SCK_Low;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SDA_Low;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SCK_High;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SDA_High;
    Delay10KTCYx(HalfBitDelay/2);
    
}

static void I2C_Write_Byte_recovery()
{
    unsigned char i;
    for(i = 0; i < 9; i++)
    {
        Set_SCK_Low;
        Delay10KTCYx(HalfBitDelay/2);
        Set_SDA_High;
        Delay10KTCYx(HalfBitDelay/2);
        Set_SCK_High;
        Delay10KTCYx(HalfBitDelay/2);
    }
    Set_SCK_Low;
    Set_SDA_High;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SCK_High;
    Delay10KTCYx(HalfBitDelay/2);

}

static void Recover_From_I2C_Hang() {
    SSP2CON1 &= 0xDF;
    Delay10KTCYx(60);
    
    InitI2C_recovery();
    I2C_Start_recovery();
    //Send the address , then command then the data
    I2C_Write_Byte_recovery();
    //bit_bang_I2C_Start();
    I2C_Stop_recovery();

    Delay10KTCYx(60);
}
// End i2c recovery code

static unsigned char ReadI2C2_Internal( void ) {
    unsigned int retry_count = 0;
    if( ((SSP2CON1&0x0F)==0x08) || ((SSP2CON1&0x0F)==0x0B) ) {	//master mode only
        SSP2CON2bits.RCEN = 1;           // enable master for 1 byte reception
    }
    while (retry_count < 9600) {
        if (SSP2STATbits.BF) {
            break;
        }
        retry_count++;
    }
    if (retry_count >= 9600) {
        i2c_read_error = 1;
    }
 
  return ( SSP2BUF );              // return with read byte 
}

static signed char WriteI2C2_Internal( unsigned char data_out )
{
    unsigned int retry_count = 0;
    SSP2BUF = data_out;           // write single byte to SSP2BUF
    if ( SSP2CON1bits.WCOL ) {      // test if write collision occurred
        return ( -1 );              // if WCOL bit is set return negative #
    } else {
        if( ((SSP2CON1&0x0F)!=0x08) && ((SSP2CON1&0x0F)!=0x0B) ) {	//slave mode only
#if defined (I2C_SFR_V1) 
            if ( ( !SSP2STATbits.R_NOT_W ) && ( !SSP2STATbits.BF ) )
#else
            if ( ( !SSP2STATbits.R_W ) && ( !SSP2STATbits.BF ) )// if R/W=0 and BF=0, NOT ACK was received
#endif 
            {
              return ( -2 );             // return NACK
            }
            else {
                return(0);				// return ACK	
            }
        }  else if( ((SSP2CON1&0x0F)==0x08) || ((SSP2CON1&0x0F)==0x0B) ) {	//master mode only
            while (retry_count < 9600) {
                if (!SSP2STATbits.BF) {
                    break;
                }
                retry_count++;
            }
            if (retry_count >= 9600) {
                i2c_write_error = 1;
            }
            IdleI2C2();                  // ensure module is idle
            if ( SSP2CON2bits.ACKSTAT ) { // test for ACK condition received
                return ( -2 );				//return NACK
            } else {
                return ( 0 );   			//return ACK
            }
        }
    }
}

static int i2c_read8_internal (
    int port,
    unsigned char dev_addr,
    unsigned char cmd,
    unsigned char *data)
{
    if (port == 2) {
        i2c_read_error = 0;
        i2c_write_error = 0;
        StartI2C2();
        IdleI2C2();
        WriteI2C2_Internal((dev_addr << 1) & 0xFE);
        IdleI2C2();
        WriteI2C2_Internal(cmd);
        IdleI2C2();

        RestartI2C2();
        IdleI2C2();
        WriteI2C2_Internal((dev_addr << 1) | 0x01);
        IdleI2C2();
        *data = ReadI2C2_Internal();
        if (i2c_read_error == 1 || i2c_write_error == 1) {
            StopI2C2();
            return 0xFF;
        }
        IdleI2C2();
        NotAckI2C2();
        IdleI2C2();
        StopI2C2();
        return 0;
    }
    return 0xFF;
}

static int i2c_write8_internal (
    int port,
    unsigned char dev_addr,
    unsigned char cmd,
    unsigned char data)
{
    if (port == 2) {
        i2c_write_error = 0;
        StartI2C2();
        IdleI2C2();
        WriteI2C2_Internal((dev_addr << 1) & 0xFE);
        if (i2c_write_error == 0) {
            IdleI2C2();
            WriteI2C2_Internal(cmd);
            if (i2c_write_error == 0) {
                IdleI2C2();
                WriteI2C2_Internal(data);
                if (i2c_write_error == 0) {
                    IdleI2C2();
                    StopI2C2();
                    return 0;
                }
            }
        }
        StopI2C2();
    }
    return 0xFF;
}

static int i2c_read_multiple_internal (
    int port,
    unsigned char dev_addr,
    unsigned char *data,
    unsigned int size)
{
    unsigned char ctrl_byte = ((dev_addr << 1) & 0xFE) | 0x01;
    if (port == 2) {
        i2c_read_error = 0;
        i2c_write_error = 0;
        StartI2C2();
        IdleI2C2();
        WriteI2C2_Internal(ctrl_byte);
        IdleI2C2();
        while (size) {
            *data++ = ReadI2C2_Internal();
            if (i2c_read_error == 1 || i2c_write_error == 1) {
                StopI2C2();
                return 0xFF;
            }
            size--;
            if (size == 0) {
                break;
            }
            IdleI2C2();
            AckI2C2();
            IdleI2C2();
        }
        IdleI2C2();
        NotAckI2C2();
        IdleI2C2();
        StopI2C2();

        return 0;
    }
    return 0xFF;
}

int i2c_write_multiple_internal (
    int port,
    unsigned char dev_addr,
    unsigned char *data,
    unsigned int size)
{
    int ret = 0xFF;
    if (port == 2) {
        ret = 0;
        i2c_write_error = 0;
        StartI2C2();
        IdleI2C2();
        WriteI2C2_Internal((dev_addr << 1) & 0xFE);
        if (i2c_write_error == 0) {
            IdleI2C2();
            while (size--) {
                WriteI2C2_Internal(*data++);
                if (i2c_write_error == 1) {
                    ret = 0xFF;
                    break;
                }
                IdleI2C2();
            }
        } else {
            ret = 0xFF;
        }
        StopI2C2();
    }
    return ret;
}

static void OpenI2C1_Priv( unsigned char sync_mode, unsigned char slew )
{
    SSP1STAT &= 0x3F;                // power on state
    SSP1CON1 = 0x00;                 // power on state
    SSP1CON2 = 0x00;                 // power on state
    SSP1CON1 |= sync_mode;           // select serial mode
    SSP1STAT |= slew;                // slew rate on/off

    I2C1_SCL = 1;               // Set SCL pin to input
    Delay10KTCYx(60);
    I2C1_SDA = 1;               // Set SDA pin to input
    Delay10KTCYx(60);

    SSP1CON1 |= SSPENB;              // enable synchronous serial port
}

static void OpenI2C2_Priv( unsigned char sync_mode, unsigned char slew )
{
    SSP2STAT &= 0x3F;                // power on state
    SSP2CON1 = 0x00;                 // power on state
    SSP2CON2 = 0x00;                 // power on state
    SSP2CON1 |= sync_mode;           // select serial mode
    SSP2STAT |= slew;                // slew rate on/off

    I2C2_SCL = 1;               // Set SCL pin to input
    Delay10KTCYx(60);
    I2C2_SDA = 1;               // Set SDA pin to input
    Delay10KTCYx(60);

    SSP2CON1 |= SSPENB;              // enable synchronous serial port
}


int Init_I2C(int port)
{
    if (port == 2)
    {
#if I2C_100K_MODE
        OpenI2C2_Priv(MASTER, SLEW_OFF);
#else
        OpenI2C2_Priv(MASTER, SLEW_ON);
#endif
        SSP2ADD = I2C_BRG_VALUE;
        return 0;
    }
    return -1;
}

int i2c_write8 (
    int port,
    unsigned char dev_addr,
    unsigned char cmd,
    unsigned char data)
{
   int ret = 0xFF;
   if(transaction_in_flight == 0){
       transaction_in_flight = 1;
       ret = i2c_write8_internal (port, dev_addr, cmd, data);
       if (ret == 0xFF) {
            Recover_From_I2C_Hang();
            Init_I2C(2);
        }
        transaction_in_flight = 0;
   }
   return ret;
}

int i2c_read8 (
    int port,
    unsigned char dev_addr,
    unsigned char cmd,
    unsigned char *data)
{
    int ret = 0xFF;
    if(transaction_in_flight == 0){
        transaction_in_flight = 1;
        ret = i2c_read8_internal(port, dev_addr, cmd, data);
        if (ret == 0xFF) {
            Recover_From_I2C_Hang();
            Init_I2C(2);
        }
        transaction_in_flight = 0;
    }
    return ret;
}

int i2c_read_multiple (
    int port,
    unsigned char dev_addr,
    unsigned char *data,
    unsigned int size)
{
    int ret = 0xFF;
    if(transaction_in_flight == 0){
         transaction_in_flight = 1;
        ret = i2c_read_multiple_internal (port, dev_addr, data, size);
        if (ret == 0xFF) {
            Recover_From_I2C_Hang();
            Init_I2C(2);
        }
         transaction_in_flight = 0;
    }
    return ret;
}

int i2c_write_multiple (
    int port,
    unsigned char dev_addr,
    unsigned char *data,
    unsigned int size)
{
    int ret = 0xFF;
    if(transaction_in_flight == 0){
        transaction_in_flight = 1;
        ret = i2c_write_multiple_internal (port, dev_addr, data, size);
        if (ret == 0xFF) {
            Recover_From_I2C_Hang();
            Init_I2C(2);
        }
        transaction_in_flight = 0;
    }
    return ret;
}
