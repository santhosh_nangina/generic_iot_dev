/*
 * File:   drv8830.c
 * Author: vamshi
 *
 * Created on 1 November, 2016, 6:12 PM
 */


#include <xc.h>
#include "drv8830.h"
#include "pic_i2c.h"
//#include "device/drv8830.h"
//#include <device/pic_i2c.h>
//#include "device/config.h"
//#include "device/utilities.h"

extern struct lock_info LOCK_INFO_DATA;
#if 1
#define DELAY_10X 0

//static void I2C_Delay(int factor)
//{
//    Delay1TCYx(I2C_SPEED/factor);
//}

#if SYS_I2C
    #define DRV8830_ADDR 0x60
#else
    //The A0 and A1 pins are grounded, that is 0. The address changes to 0xC0
    //If they were floating then the address is 0xC8.
    #define DRV8830_ADDR 0xEC
#endif

#define DRV8830_REG_FAULT   0x01
#define DRV8830_REG_CONTROL 0x00
#define DRV8830_PORT 2

#define SDA     PORTDbits.RD9 //TRISDbits.TRISD9       //RD9
#define SCK     PORTDbits.RD10 //TRISDbits.TRISD10      //RD10




void InitI2C(void)
{
    TRISDbits.TRISD9 = 0;
    TRISDbits.TRISD10 = 0;
    Set_SDA_High;
    Set_SCK_High;

}

static void I2C_Start()
{
    Set_SCK_High;
    Set_SDA_High;
    
//    LATDbits.LATD10 = 0;
 //   LATDbits.LATD9 = 1;
    //Set_SCK_Low;
    Set_SDA_Low;
    
}

static void I2C_Stop(void)
{
    Set_SCK_Low;
    Set_SDA_Low;
    Set_SCK_High;
    Set_SDA_High;

}

//The below code (3 functions) is unused and untested.
//Need to test it thoroughly before
#if 0

static void I2C_ReStart(void)
{
    Set_SCK_Low;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SDA_High;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SCK_High;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SDA_Low;
    Delay10KTCYx(HalfBitDelay/2);
    
}
#endif

static unsigned char I2C_Read_Byte(void)
{
    unsigned char i =0, RxData = 0,j,z;
    //Set_SDA_High;
    //TRISDbits.TRISD9 = 1;
    for (i=0; i<20; i++){
        for (j=0; j<100; j++){
             
        }
    }
    for(i = 0; i < 8; i++)
    {   
        SCK = 1;
        //Set_SCK_High;
        i2c_dly();
        RxData = RxData | (SDA << (7-i));
        SCK = 0;
        //Set_SCK_Low;
    }
//    for(i = 0; i < 8; i++)
//    {   
//        RxData <<= 1;
//        SCK = 1;
//        //Set_SCK_High;
//        i2c_dly();
//        RxData |= SDA;
//        i2c_dly();
//        SCK = 0;
//        //Set_SCK_Low;
//    }
    return RxData; 

}

static void I2C_Send_ACK(void)
{
    Set_SCK_Low;
    
    Set_SDA_Low;

    Set_SCK_High;
    
}
#if 0
static void I2C_Send_NACK(void)
{
    Set_SCK_Low;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SDA_High;
    Delay10KTCYx(HalfBitDelay/2);
    Set_SCK_High;
    Delay10KTCYx(HalfBitDelay/2);
    
}
#endif


static void I2C_Write_Byte(unsigned char Byte)
{
    unsigned char i;
    for(i = 0; i < 8; i++)
    {
        Set_SCK_Low;
        SYS_TMR_DelayMS ( 100 );
    
        if( (Byte << i)& (0x80) )
            SDA = 1;
        else
            SDA = 0;
        SYS_TMR_DelayMS ( 100 );

        Set_SCK_High;

        SYS_TMR_DelayMS ( 100 );
    }
    Set_SCK_Low;
//    //Set_SDA_High;
//    Set_SCK_High;
////    Set_SDA_Low;
//    Set_SCK_Low;

}

void i2c_dly(void)
{
}

unsigned char i2c_data[10];
void I2C_ReadTransaction(unsigned char data)
{
   	unsigned char inData;
    int i=0, j=0, z=0;
	/* Start */
  	I2C_Start(); 
//	/* Slave address */
   	I2C_Write_Byte(DRV8830_ADDR);
    I2C_Send_ACK();
    
    for (j=0; j<256; j++){
            z=0;
    }
//	/* Slave control byte */
   	I2C_Write_Byte(data);
    I2C_Send_ACK();
    
    for (j=0; j<256; j++){
            z=0;
    }
//	/* Stop */
   	I2C_Stop();
    I2C_Stop();
    for (i=0; i<256; i++){
        for (j=0; j<256; j++){
            z=0;
        }
    }
//	/* Start */
   	I2C_Start();

	/* Slave address + read */
   	I2C_Write_Byte(DRV8830_ADDR | 1);
    i2c_dly();
    I2C_Send_ACK();
    Set_SCK_Low;
    for (j=0; j<150; j++){
            z=0;
    }
	/* Read */
     i2c_dly();
	inData = I2C_Read_Byte();
//    SYS_TMR_DelayMS ( 50 );
    
//    I2C_Stop();
    if(inData != 0){
        i2c_data[0] = inData;
        return inData;                 
    }
}

#else

void i2c_dly(void)
{
}

void i2c_start(void)
{
  SDA = 1;             // i2c start bit sequence
  i2c_dly();
  SCL = 1;
  i2c_dly();
  SDA = 0;
  i2c_dly();
  SCL = 0;
  i2c_dly();
}

void i2c_stop(void)
{
  SDA = 0;             // i2c stop bit sequence
  i2c_dly();
  SCL = 1;
  i2c_dly();
  SDA = 1;
  i2c_dly();
}

unsigned char i2c_rx(char ack)
{
char x, d=0;
  SDA = 1; 
  for(x=0; x<8; x++) {
    d <<= 1;
    do {
      SCL = 1;
    }
    while(SCL_IN==0);    // wait for any SCL clock stretching
    i2c_dly();
    if(SDA_IN) d |= 1;
    SCL = 0;
  } 
  if(ack) SDA = 0;
  else SDA = 1;
  SCL = 1;
  i2c_dly();             // send (N)ACK bit
  SCL = 0;
  SDA = 1;
  return d;
}

char i2c_tx(unsigned char d)
{
char x;
char b;
  for(x=8; x; x--) {
    if(d&0x80) SDA = 1;
    else SDA = 0;
    SCL = 1;
    d <<= 1;
    SCL = 0;
  }
  SDA = 1;
  SCL = 1;
  i2c_dly();
  b = SDA_IN;          // possible ACK bit
  SCL = 0;
  return b;
}

void I2C_ReadTransaction(unsigned char data)
{
    SDA = 1;
    SCL = 1;

    i2c_start();              // send start sequence
    SCL = 1;
    i2c_tx(0xEC);             // SRF08 I2C address with R/W bit clear
    SCL = 1;
    i2c_tx(0xD0);             // SRF08 light sensor register address
    SCL = 1;
    i2c_start();              // send a restart sequence
    SCL = 1;
    i2c_tx(0xE1);             // SRF08 I2C address with R/W bit set
    SCL = 1; 
    i2c_stop();               // send stop sequence
}
#endif