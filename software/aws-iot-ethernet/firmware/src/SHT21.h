/**
 * @file SHT21.h
 * @author Firmware department
 * @date 16 Mar 2016
 * @brief H File which contains every functions to use the temperature and humidity
 * sensor SHT21.
 * 
 * @see http://www.argotec.es
 */ 

#ifndef SHT21_H
#define	SHT21_H

#if defined(__XC16)
    #include "Hardwareprofile.h"
    #include "I2C.h"
    #include "Delay.h"
    #include <GenericTypeDefs.h>
#elif defined(__XC32)
    #include "framework/driver/i2c/drv_i2c_static.h"
    #include "TimeDelay.h"
    #include <GenericTypeDefs.h>
#endif

#define SHT21_Address        0x80

#define eTempHoldCmd         0xE3
#define eRHumidityHoldCmd    0xE5
#define eTempNoHoldCmd       0xF3
#define eRHumidityNoHoldCmd  0xF5

typedef struct SHT21_defs {
    float Temperature;
    float Humidity;
} SHT21_t; ///< This struct contains the SHT21 values.

// SHT21 structs info
extern SHT21_t SHT21Data_Inf;

/** @ingroup CORE01070 */
/*@{*/
    /** @defgroup Drivers */
    /*@{*/
        /** @defgroup SHT21 */
        /*@{*/

/**
 * Function to start the SHT21 sensor
 * 
 * <VAR><B> - CORE01070 parameters: </B></VAR>
 * @param  None (but it uses the I2C number 3)
 * 
 * @return This function returns an error message if something was wrong.
 */

void SHT21Init(void);

/**
 * Function to obtain one value: temperature or humidity
 * 
 * <VAR><B> - CORE01070 parameters: </B></VAR>
 * @param  Temp_Hum -> It is the value desired, and the available values are:
 * - eTempHoldCmd or 0xE3\r\n
 * - eRHumidityHoldCmd or 0xE5\r\n
 * @param  raw -> It gives the possibility to obtain the result in raw or in 
 * temperature/humidity (TRUE -> raw, FALSE -> temperature/humidity).
 * 
 * @return This function can return two kinds of values:
 * - The current value of the temperature/humidity selected.
 * - An error message if something was wrong.
 */

float SHT21GetValue(UINT8 Temp_Hum, BOOL raw);

/**
 * Function to obtain both temperature and humidity
 * 
 * <VAR><B> - CORE01070 parameters: </B></VAR>
 * @param  raw -> It gives the possibility to obtain the result in raw or in 
 * voltage (TRUE -> raw, FALSE -> voltage).
 * 
 * @return This function can return two kinds of values:
 * - An SHT21_t struct which contains both temperature and humidity.
 * - An error message if something was wrong.
 */

SHT21_t SHT21GetEveryValues(BOOL raw); 

        /*@}*/
    /*@}*/
/*@}*/

/** @ingroup CORE01070 */
/*@{*/
    /** @defgroup Drivers */
    /*@{*/
        /** @defgroup SHT21 */
        /*@{*/

        /*@}*/
    /*@}*/
/*@}*/

#endif	/* SHT21_H */

