/**************************************************************************************************
* File: Weather click.c
* File Type: C - Source Code File
* Project Name: Weather click
* Company: (c) mikroElektronika, 2015
* Revision History:
*       - initial release (VM);
* Description:
*Weather click carries the BME280 integrated environmental unit from Bosch.
*It�s a sensor that detects humidity, pressure and temperature, specifically designed for low current consumption and long term stability.
*Weather click can communicate with the target MCU either through mikroBUS� SPI or I2C interfaces. 
*The board is designed to use a 3.3V power supply.
* Test configuration:
*    MCU:             STM32F107VG
*                     http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/DM00037051.pdf
*    dev.board:       EasyMx PRO for STM32
*                     http://www.mikroe.com/eng/products/view/869/mikromedia-for-stm32-m4/
*    Oscillator:      HSI-PLL, 72.000MHz
*    EXT:             Weather click:
*                      http://www.mikroe.com/click/weather
*    SW:              mikroC PRO for ARM
*                     http://www.mikroe.com/eng/products/view/752/mikroc-pro-for-arm/
* NOTES:
*     I2C bus is used for communication with the click.
*/

#include "BME280_Driver.h"
#include "BME280_Defs.h"
#include "resources.h"
#include "Weather click.h"

char tmp;
char text[20];
float tmp_f;

static void Display_Values() {
    tmp_f = BME280_GetTemperature();
    tmp_f = BME280_GetPressure();
}

void BME280_INIT() {
    BME280_SetStandbyTime(BME280_STANDBY_TIME_1_MS);                              // Standby time 1ms
    BME280_SetFilterCoefficient(BME280_FILTER_COEFF_16);                          // IIR Filter coefficient 16
   // BME280_SetOversamplingPressure(BME280_OVERSAMP_16X);                          // Pressure x16 oversampling
    BME280_SetOversamplingTemperature(BME280_OVERSAMP_2X);                        // Temperature x2 oversampling
  //  BME280_SetOversamplingHumidity(BME280_OVERSAMP_1X);                           // Humidity x1 oversampling
    BME280_SetOversamplingMode(BME280_NORMAL_MODE);
}

void weather1() {

  //BME280_GetID();

  BME280_ReadCalibrationParams();                                               //Read calibration parameters
  
//  BME280_SetOversamplingPressure(BME280_OVERSAMP_1X);
//  BME280_SetOversamplingTemperature(BME280_OVERSAMP_1X);
//  BME280_SetOversamplingHumidity(BME280_OVERSAMP_1X);
//  BME280_SetOversamplingMode(BME280_FORCED_MODE);
//  
//  while(BME280_IsMeasuring());
//  BME280_ReadMeasurements();
//  //////////////////////////--------------------- aksjdfh---------------------///////////////////
//  BME280_INIT();
//  
//  while(1) {
//     while(BME280_IsMeasuring());
//     BME280_ReadMeasurements();
//     Display_Values();
//     BME280_INIT();
//     //delay_ms(1500);
//  }
  
}
void weather2(){

    I2C_ReadRegister(BME280_TEMPERATURE_CALIB_DIG_T3_LSB_REG, 2);
}

void setoversamplingTemperature(){

    BME280_SetOversamplingTemperature(BME280_OVERSAMP_1X);     // Temperature x1 oversampling

}

void SetoverSamplingTemperature(){
    BME280_SetOversamplingTemperature(BME280_OVERSAMP_2X);    // Temperature x2 oversampling
}


void set_mode(){

    BME280_SetOversamplingMode(BME280_FORCED_MODE);

}

void set_normal_mode(){
    BME280_SetOversamplingMode(BME280_NORMAL_MODE);
}

void set_sleep_mode(){
    I2C_WriteRegister(BME280_CTRL_MEAS_REG, 0x00);
    I2C_ReadRegister(BME280_CTRL_MEAS_REG, 1);
}

void read_the_mode(){
    I2C_ReadRegister(BME280_CTRL_MEAS_REG, 1);
}

void set_the_config(){
    char dataToWrite;
    char standby = 1;
    char filter = 1;
	dataToWrite = (standby << 0x5) & 0xE0;
	dataToWrite |= (filter << 0x02) & 0x1C;
	I2C_WriteRegister(BME280_CONFIG_REG, dataToWrite);
}

void delay(){
    int i, j;
    for(i=0; i<255; i++){
        for(j=0; j<255; j++){
        }
    }
}