/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "app1.h"
#include "parson.h"
#include "bsp_config.h"
#include "app_nvm_support.h"
#include "wolfmqttsdk/wolfmqtt/mqtt_client.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;
extern APP1_DATA app1Data;

char topic_switch_1[128];
char topic_switch_2[128];
char topic_switch_3[128];
char topic_switch_4[128];

char topic_pot[128];
char topic_lwt[128];

char topic_led_1[128];
char topic_led_2[128];
char topic_led_3[128];
char topic_led_4[128];

#define MQTT_DEFAULT_CMD_TIMEOUT_MS 10000
#define MAX_BUFFER_SIZE 1024
#define MAX_PACKET_ID 65536
#define KEEP_ALIVE 60
#define TOKEN "CKEPZOMt7sihvhaJ6BhZZ0MI"
#define JENKINS 0
byte txBuffer[MAX_BUFFER_SIZE];
byte rxBuffer[MAX_BUFFER_SIZE];

#define SDA  PORTDbits.RD9 
#define SCL  PORTDbits.RD10

static int mPacketIdLast;
char buffer2[512];
char buffer4[512];
char buffer3[1024];
char buffer5[1024];
char buffer[1024];
char buffer1[512];
uint32_t sysCount = 1;
int mqtt_started = 0;
int mqtt_reconnect = 0;
uint64_t sec;
uint8_t t = 1;
uint64_t msec;
extern getTime;
extern uint32_t dwSNTPSeconds;
char ConnectPayload[MAX_BUFFER_SIZE];
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f)  MACRO_NAME(f)
uint32_t addr_temp;
IPV4_ADDR send_addr;
TCPIP_NET_HANDLE    netH;
int i, nNets;
const uint8_t *MacAddr;
uint8_t nullMacAddr[6];
extern char chip_id[20];
extern char tmp_data[20];
extern char RxBuffer[20];
char bme_status;
extern char value;
uint32_t adc_t, adc_t1;
float tmp_f;
uint8_t f_temp;
char select_state = APP_STATE_IDLE;
// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

// WolfMQTT Callbacks for network connectivity

void tmrcallback( uintptr_t context, uint32_t currtick)
{
    uint32_t timeout = 0;
    appData.state = set_config;
    select_state = SamplingTemperature;
}

int APP_tcpipConnect_cb(void *context, const char* host, word16 port, int timeout_ms)
{
    uint32_t timeout = 0;
    timeout = SYS_TMR_TickCountGet();
    SYS_CONSOLE_PRINT("App:  DNS:   Resolving host '%s'\r\n", &appData.host1);
    TCPIP_DNS_RESULT dnsResult;
    
    dnsResult = TCPIP_DNS_Resolve((const char *)appData.host1, TCPIP_DNS_TYPE_A);
    if(dnsResult < 0)
    {
        SYS_CONSOLE_MESSAGE("App:  DNS:  Failed to begin\r\n");
        return APP_CODE_ERROR_FAILED_TO_BEGIN_DNS_RESOLUTION;
    }

    while((dnsResult = TCPIP_DNS_IsResolved((const char *)appData.host1, &appData.host_ipv4, IP_ADDRESS_TYPE_IPV4)) == TCPIP_DNS_RES_PENDING)
    {
        if(APP_TIMER_Expired_ms(&timeout, timeout_ms))
        {
            return APP_CODE_ERROR_CMD_TIMEOUT;
        }
    }
    if(dnsResult != (TCPIP_DNS_RES_OK))
    {
        SYS_CONSOLE_PRINT("App:  DNS:  Resolution failed - Aborting\r\n");
        return APP_CODE_ERROR_DNS_FAILED;
    } 
    else if(dnsResult == TCPIP_DNS_RES_OK)
    {
       SYS_CONSOLE_PRINT("App:  DNS:  Resolved IPv4 Address: %d.%d.%d.%d for host '%s'\r\n", 
            appData.host_ipv4.v4Add.v[0],appData.host_ipv4.v4Add.v[1],appData.host_ipv4.v4Add.v[2],
            appData.host_ipv4.v4Add.v[3],appData.host1);
    }  
    SYS_CONSOLE_PRINT("App:  TCPIP:  Opening socket to '%d.%d.%d.%d:%d'\r\n", 
        appData.host_ipv4.v4Add.v[0], appData.host_ipv4.v4Add.v[1], appData.host_ipv4.v4Add.v[2],
        appData.host_ipv4.v4Add.v[3], appData.port);
    
    uint32_t timeSocketbefore = SYS_TMR_TickCountGet();
    appData.socket = NET_PRES_SocketOpen(0, NET_PRES_SKT_UNENCRYPTED_STREAM_CLIENT, IP_ADDRESS_TYPE_IPV4, (NET_PRES_SKT_PORT_T)port, (NET_PRES_ADDRESS *)&appData.host_ipv4, (NET_PRES_SKT_ERROR_T*)&appData.error);
    NET_PRES_SocketWasReset(appData.socket);
    
    if(appData.socket == INVALID_SOCKET)
    {
        SYS_CONSOLE_MESSAGE("App:  TCPIP:  Invalid socket error\r\n");
        NET_PRES_SocketClose(appData.socket);
        return APP_CODE_ERROR_INVALID_SOCKET;
    }
 
    if(!NET_PRES_SKT_IsConnected(appData.socket))
    {
        if(APP_TIMER_Expired_ms(&timeout, timeout_ms))
        {
            return APP_CODE_ERROR_CMD_TIMEOUT;
        }
    }
    
    uint32_t timeSocketafter = SYS_TMR_TickCountGet();
    SYS_CONSOLE_PRINT("App:  Socket Opened - Time to open %d ticks\r\n", timeSocketafter-timeSocketbefore);
    
    return 0; //Success
}

int APP_tcpipWrite_cb(void *context, const byte* buf, int buf_len, int timeout_ms)
{
    int ret = 0;
    uint32_t timeout;
    
    APP_TIMER_Set(&timeout);
    //wait for data to be read, or error, or timeout
    while(NET_PRES_SocketWriteIsReady(appData.socket, buf_len, 0) == 0)
    {
        if(NET_PRES_SocketWasReset(appData.socket))
        {
            ret = APP_CODE_ERROR_SSL_FATAL;
            return ret;
        }
        if(APP_TIMER_Expired_ms(&timeout, (uint32_t)timeout_ms))
        {
            ret = APP_CODE_ERROR_CMD_TIMEOUT;
            return ret;
        }
    }
    ret = NET_PRES_SocketWrite(appData.socket, (uint8_t*)buf, buf_len);
    return ret;
}

int APP_tcpipRead_cb(void *context, byte* buf, int buf_len, int timeout_ms)
{
    int ret = 0;
    uint32_t timeout;
    
    APP_TIMER_Set(&timeout);
    // Wait for data to be read, or error, or timeout
    while(NET_PRES_SocketReadIsReady(appData.socket) == 0)
    {
        if(NET_PRES_SocketWasReset(appData.socket))
        {
            ret = APP_CODE_ERROR_SSL_FATAL;
            return ret;
        }
        if(APP_TIMER_Expired_ms(&timeout, (uint32_t)timeout_ms))
        {
            ret = APP_CODE_ERROR_CMD_TIMEOUT;
            return ret;
        }
    }
    ret = NET_PRES_SocketRead(appData.socket, (uint8_t*)buf, buf_len);
    return ret;
}

int APP_tcpipDisconnect_cb(void *context)
{
    int ret = 0;
    int rc = MqttClient_Disconnect(&appData.myClient);
    NET_PRES_SKT_Close(appData.socket);
//    TCPIP_TCP_Disconnect(appData.socket_1);
//    TCPIP_TCP_Close(appData.socket_1);
    appData.state = APP_TCPIP_MQTT_NET_CONNECT;
    return ret;
}

static word16 mqttclient_get_packetid(void)
{
    mPacketIdLast = (mPacketIdLast >= MAX_PACKET_ID) ?
        1 : mPacketIdLast + 1;
    return (word16)mPacketIdLast;
}

const char* APP_Switch_Publish_Helper(BSP_SWITCH_ENUM sw)
{
    switch (sw)
    {
        case BSP_SWITCH_1:
            return "state.reported.button1";
        case BSP_SWITCH_2:
            return "state.reported.button2";
        case BSP_SWITCH_3:
            return "state.reported.button3";
        case BSP_SWITCH_4:
            return "state.reported.button4";
        default:
            return 0;
            break;
    }
}

int mqttclient_message_cb(MqttClient *client, MqttMessage *msg, byte msg_new, byte msg_done)
{
        char payload[MAX_BUFFER_SIZE];
        memcpy(payload, msg->buffer, msg->total_len);
        payload[msg->total_len] = '\0';
        SYS_CONSOLE_PRINT("\r\nApp:  MQTT.Message Received: %s -- Topic %*.*s\r\n\r\n", payload, msg->topic_name_len, msg->topic_name_len, msg->topic_name);
        appData.lightShowVal = BSP_LED_RX;
        xQueueSendToFront(app1Data.lightShowQueue, &appData.lightShowVal, 1);
    
        // If the topic matches our Topic_led_1 topic
        if(strncmp(topic_led_1, msg->topic_name, strlen(topic_led_1)) == 0)
        {
            appData.led1 = true;
            if(strcmp(payload, "ON") == 0)
            {
                BSP_LEDOn(BSP_LED_1_CHANNEL, BSP_LED_1_PORT);
                appData.led1val = true;
            }
            else if (strcmp(payload, "OFF") == 0)
            {
                BSP_LEDOff(BSP_LED_1_CHANNEL, BSP_LED_1_PORT);
                appData.led1val = false;
            }
        }
        if(strncmp(topic_led_2, msg->topic_name, strlen(topic_led_2)) == 0)
        {
            appData.led2 = true;
            if(strcmp(payload, "ON") == 0)
            {
                BSP_LEDOn(BSP_LED_2_CHANNEL, BSP_LED_2_PORT);
                appData.led2val = true;
            }
            else if (strcmp(payload, "OFF") == 0)
            {
                BSP_LEDOff(BSP_LED_2_CHANNEL, BSP_LED_2_PORT);
                appData.led2val = false;
            }
        }
        if(strncmp(topic_led_3, msg->topic_name, strlen(topic_led_3)) == 0)
        {
            appData.led3 = true;
            if(strcmp(payload, "ON") == 0)
            {
                BSP_LEDOn(BSP_LED_3_CHANNEL, BSP_LED_3_PORT);
                appData.led3val = true;
            }
            else if (strcmp(payload, "OFF") == 0)
            {
                BSP_LEDOff(BSP_LED_3_CHANNEL, BSP_LED_3_PORT);
                appData.led4val = false;
            }
        }
        if(strncmp(topic_led_4, msg->topic_name, strlen(topic_led_4)) == 0)
        {
            appData.led4 = true;
            if(strcmp(payload, "ON") == 0)
            {
                BSP_LEDOn(BSP_LED_4_CHANNEL, BSP_LED_4_PORT);
                appData.led4val = true;
            }
            else if (strcmp(payload, "OFF") == 0)
            {
                BSP_LEDOff(BSP_LED_4_CHANNEL, BSP_LED_4_PORT);
                appData.led4val = false;
            }
        }
    return 0;
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


bool APP_TIMER_Expired(uint32_t * timer, uint32_t seconds)
{
    if((SYS_TMR_TickCountGet() - *timer) > (seconds * 1000))
    {
        return true;
    }
    else
    {
        return false;
    }
    return false;
}

bool APP_TIMER_Expired_ms(uint32_t * timer, uint32_t mseconds)
{
    if((SYS_TMR_TickCountGet() - *timer) > (mseconds)) 
    {
        return true;
    }
    else
    {
        return false;
    }
    return false;
}

bool APP_TIMER_Set(uint32_t * timer)
{
    *timer = SYS_TMR_TickCountGet();
    return true;
}

const char* APP_ReturnCodeToString(int return_code)
{
    switch(return_code)
    {
        case APP_CODE_SUCCESS:
            return "Success";
        case APP_CODE_ERROR_BAD_ARG:
            return "Error (Bad argument)";
        case APP_CODE_ERROR_OUT_OF_BUFFER:
            return "Error (Out of buffer)";
        case APP_CODE_ERROR_SSL_FATAL:
            return "Error (SSL Fatal)";
        case APP_CODE_ERROR_INVALID_SOCKET:
            return "Error (Invalid Socket)";
        case APP_CODE_ERROR_FAILED_TO_BEGIN_DNS_RESOLUTION:
            return "Error (Failed to Begin DNS)";
        case APP_CODE_ERROR_DNS_FAILED:
            return "Error (DNS Failed)";
        case APP_CODE_ERROR_FAILED_SSL_NEGOTIATION:
            return "Error (Failed SSL Negotiation)";
        case APP_CODE_ERROR_TIMEOUT:
            return "Error (Timeout)";
        case APP_CODE_ERROR_CMD_TIMEOUT:
            return "Error (Command Timeout)";
    }
    return "Unknown";
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */
extern  DRV_I2C_INIT drvI2C0InitData;
void APP_Initialize ( void )
{
#if JENKINS
    if(strcmp(MACRO_VALUE(HEX_TOKEN), "CKEPZOMt7sihvhaJ6BhZZ0MI") == 0){
        BSP_LEDOn(BSP_LED_1_CHANNEL, BSP_LED_1_PORT);
    }
    appData.ProToken1 = MACRO_VALUE(HEX_TOKEN);
    strcpy(appData.ProToken, appData.ProToken1);
    if(strcmp(appData.ProToken,  "CKEPZOMt7sihvhaJ6BhZZ0MI") == 0){
        BSP_LEDOff(BSP_LED_1_CHANNEL, BSP_LED_1_PORT);
    }
    
    appData.HOST = MACRO_VALUE(IP);
    strcpy(appData.host_1, appData.HOST);
    strcpy(appData.host1, appData.HOST);
    if(strcmp(appData.host_1, "192.168.2.65") == 0){
        BSP_LEDOn(BSP_LED_2_CHANNEL, BSP_LED_2_PORT);
    }
    
    appData.SCHEME1 = MACRO_VALUE(SCHEME);
    strcpy(appData.SCHEME, appData.SCHEME1);
    if(strcmp(appData.SCHEME1, "http") == 0){
        BSP_LEDOn(BSP_LED_3_CHANNEL, BSP_LED_3_PORT);
    }
    appData.port_1 = atoi(MACRO_VALUE(PORT));
    if(appData.port_1 == 80){
        BSP_LEDOff(BSP_LED_4_CHANNEL, BSP_LED_4_PORT);
    }
#else
    appData.ProToken1 = "CKEPZOMt7sihvhaJ6BhZZ0MI";
    strcpy(appData.ProToken,  appData.ProToken1);
    appData.HOST = "192.168.2.65";
    strcpy(appData.host1, appData.HOST);
    strcpy(appData.host_1, appData.HOST);
    appData.SCHEME1 = "http";
    strcpy(appData.SCHEME, appData.SCHEME1);
    appData.port_1 = 80;
#endif
    
    appData.state = APP_STATE_INIT;
    memset(appData.host, '\0', sizeof(appData.host));
    appData.port = AWS_IOT_PORT;
    
    // Initialize MQTT net callbacks
    appData.myNet.connect = APP_tcpipConnect_cb;
    appData.myNet.disconnect = APP_tcpipDisconnect_cb;
    appData.myNet.read = APP_tcpipRead_cb;
    appData.myNet.write = APP_tcpipWrite_cb;
    
    // Init LED publish bools
    appData.led1 = false;
    appData.led2 = false;
    appData.led3 = false;
    appData.led4 = false;
    
    appData.led1val = false;
    appData.led2val = false;
    appData.led3val = false;
    appData.led4val = false;
    
    
}

/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Tasks ( void )
{   
    static int validConfig = 0;
    /* Check the application's current state. */
    APP_TEMP_Tasks();
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            bool appInitialized = true;
            if (appInitialized)
            {
                SYS_CONSOLE_MESSAGE("App:  Initialized\r\n");
                appData.state = APP_NVM_MOUNT_DISK;
                //appData.state = Get_id;
            }
            break;
        }
        // Mount the file system where the webpages are loaded
        case APP_NVM_MOUNT_DISK:
        {
            if(SYS_FS_Mount(SYS_FS_NVM_VOL, LOCAL_WEBSITE_PATH_FS, MPFS2, 0, NULL) == 0)
            {
                SYS_CONSOLE_PRINT("App:  The %s File System is mounted.\r\n", SYS_FS_MPFS_STRING);
                appData.state = APP_NVM_ERASE_CONFIGURATION;
            }
            else
            {   // Timeout 5 seconds
                if(APP_TIMER_Expired(&appData.genericUseTimer, 60))
                {
                    SYS_CONSOLE_PRINT("App:  The %s File System failed to mount.  Critical Error, reset board\r\n", SYS_FS_MPFS_STRING);
                    appData.lightShowVal = BSP_LED_NVM_FAILED_MOUNT;
                    xQueueSendToFront(app1Data.lightShowQueue, &appData.lightShowVal, 1);  
                    while(1);
                }
            }
            break;
        }    
        
        // If user presses switch 2 and 3 on power up, the configuration will be erased
        case APP_NVM_ERASE_CONFIGURATION:
        {
            if((BSP_SWITCH_StateGet(BSP_SWITCH_3_CHANNEL, BSP_SWITCH_3_PORT) == BSP_SWITCH_STATE_ASSERTED) 
             && (BSP_SWITCH_StateGet(BSP_SWITCH_2_CHANNEL, BSP_SWITCH_2_PORT) == BSP_SWITCH_STATE_ASSERTED))
            {
                memset(appData.host, 0, sizeof(appData.host));
                APP_NVM_Write(NVM_HOST_ADDRESS_SPACE, appData.host);
                APP_NVM_Erase(NVM_CLIENT_CERTIFICATE_SPACE);
                APP_NVM_Erase(NVM_CLIENT_KEY_SPACE);
                SYS_CONSOLE_MESSAGE("***************************************\r\n"
                                    "App:  Erasing configuration!\r\n"
                                    "***************************************\r\n");
                appData.state = APP_TCPIP_WAIT_INIT;
                break;
            }
            appData.state = APP_NVM_LOAD_CONFIGURATION;
            break;
        }
        
        // Load the configuration stored in NVM on powerup
        case APP_NVM_LOAD_CONFIGURATION:
        {
            SYS_CONSOLE_MESSAGE("App:  Loading AWS IoT Endpoint Address and AWS Certificate/Certificate Private Key\r\n");
            APP_NVM_Read(NVM_HOST_ADDRESS_SPACE, appData.host, sizeof(appData.host));
            APP_NVM_Read(NVM_CLIENT_CERTIFICATE_SPACE, appData.clientCert, sizeof(appData.clientCert));
            APP_NVM_Read(NVM_CLIENT_KEY_SPACE, appData.clientKey, sizeof(appData.clientKey));
            if(appData.host[0] != '\0')
            {   // Set this flag so we know we loaded a valid config from NVM
                validConfig = 1;
            }
            appData.state = APP_TCPIP_WAIT_INIT;
            break;
        }
        
        // Wait for the TCPIP stack to initialize, store the boards MAC address and initialize mDNS service
        case APP_TCPIP_WAIT_INIT:
        {
            SYS_STATUS          tcpipStat;
            TCPIP_NET_HANDLE    netH;
            int                 i, nNets;
            
            
            tcpipStat = TCPIP_STACK_Status(sysObj.tcpip);
            if(tcpipStat < 0) 
            {   // some error occurred
                break;
            }
            else if(tcpipStat == SYS_STATUS_READY) 
            {
                // now that the stack is ready we can check the
                // available interfaces
                nNets = TCPIP_STACK_NumberOfNetworksGet();
                for(i = 0; i < nNets; i++) 
                {
                    netH = TCPIP_STACK_IndexToNet(i);
                    TCPIP_STACK_NetNameGet(netH);
                    TCPIP_STACK_NetBIOSName(netH);
             
                    // Retrieve MAC Address for UUID
                    TCPIP_NET_HANDLE netH = TCPIP_STACK_NetHandleGet("PIC32INT");
                    TCPIP_MAC_ADDR* pAdd = 0;
                    pAdd = (TCPIP_MAC_ADDR *)TCPIP_STACK_NetAddressMac(netH);
                    
                    // Store UUID for application
                    appData.macAddress.v[5] = pAdd->v[5];
                    appData.macAddress.v[4] = pAdd->v[4];
                    appData.macAddress.v[3] = pAdd->v[3];
                    appData.macAddress.v[2] = pAdd->v[2];
                    appData.macAddress.v[1] = pAdd->v[1];
                    appData.macAddress.v[0] = pAdd->v[0];
                    
                    // Convert to string
                    sprintf(appData.uuid, "%02x%02x%02x%02x%02x%02x",
                            appData.macAddress.v[0], appData.macAddress.v[1], appData.macAddress.v[2],
                            appData.macAddress.v[3], appData.macAddress.v[4], appData.macAddress.v[5]); 
                    
                    char mDNSServiceName[16]; // base name of the service Must not exceed 16 bytes long
                    strcpy(mDNSServiceName, &appData.uuid[6]); //Copy over UUID last 6 characters, 
                    strcat(mDNSServiceName, "_IoT-E");
                    SYS_CONSOLE_PRINT("App:  Registering mDNS service as '%s'\r\n", mDNSServiceName);
                    // mDNS name will be xxxxxx_IoT-E where "xxxxxx" is the last three bytes of MAC address
                    mDNSServiceName[sizeof(mDNSServiceName) - 2] = '1' + i;
                    TCPIP_MDNS_ServiceRegister( netH
                            , mDNSServiceName                     // name of the service
                            ,"_http._tcp.local"                   // type of the service
                            ,80                                   // TCP or UDP port, at which this service is available
                            ,((const uint8_t *)"path=/index.htm") // TXT info
                            ,1                                    // auto rename the service when if needed
                            ,NULL                                 // no callback function
                            ,NULL);      
                }             
                
                APP_TIMER_Set(&appData.genericUseTimer);
                appData.state = APP_TCPIP_WAIT_FOR_IP;
            }
            break;
        }

        case APP_TCPIP_WAIT_FOR_IP:
        {
            IPV4_ADDR           ipAddr;
            TCPIP_NET_HANDLE    netH;
            int i, nNets;
            
            if(APP_TIMER_Expired(&appData.genericUseTimer, 30))
            {
                SYS_CONSOLE_MESSAGE("App:  Not getting IP Addr, check connections.  Retrying...\r\n");
                APP_TIMER_Set(&appData.genericUseTimer);
            }
                
            nNets = TCPIP_STACK_NumberOfNetworksGet();
            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                ipAddr.Val = TCPIP_STACK_NetAddress(netH);
                if( 0 != ipAddr.Val)
                {
                    if (ipAddr.v[0] != 0 && ipAddr.v[0] != 169) // Wait for a Valid IP
                    {
                        uint32_t lightShowVal = BSP_LED_EASY_CONFIGURATION;
                        xQueueSendToFront(app1Data.lightShowQueue, &lightShowVal, 1);
                        SYS_CONSOLE_PRINT("App:  Board online.  mDNS online. IP addr %d.%d.%d.%d online.  All systems nominal.\r\n",
                        ipAddr.v[0],ipAddr.v[1],ipAddr.v[2],ipAddr.v[3]);
                        SYS_CONSOLE_PRINT("App:  AWS Thing Name (MAC Address) '%s'\r\n", appData.uuid);
                        SYS_CONSOLE_MESSAGE("App:  Waiting for configuration...\r\n");
                        appData.state = APP_GET_NTP_TIME;                        
                    }
                }
            }
            break;
        }
        case APP_GET_NTP_TIME:
        {
            if(getTime == 1 && mqtt_reconnect == 1)
            {
                sec = TCPIP_SNTP_UTCSecondsGet();
                msec = sec*1000;
                appData.state = APP_TCPIP_MQTT_PROTOCOL_CONNECT;
            }
            else if (getTime == 0)
            {
                appData.state = APP_GET_NTP_TIME;
            }
            else if (getTime == 1 && mqtt_reconnect == 0)
            {
                sec = TCPIP_SNTP_UTCSecondsGet();
                msec = sec*1000;
                appData.state = APP_TCPIP_OPEN_SOCKET;
            }
            break;
        } 

        case APP_TCPIP_OPEN_SOCKET:
        {      
            TCPIP_Helper_StringToIPAddress("192.168.2.65", &send_addr);
//            appData.host_1 = "iot.cariboutech.com";            
            uint32_t timeout = 0;
            TCPIP_DNS_RESULT dnsResult;
            timeout = SYS_TMR_TickCountGet();
            SYS_CONSOLE_PRINT("App:  DNS:   Resolving host '%s'\r\n", &appData.host_1);
            dnsResult = TCPIP_DNS_Resolve((const char *)appData.host_1, TCPIP_DNS_TYPE_A);
            if(dnsResult < 0)
            {
                SYS_CONSOLE_MESSAGE("App:  DNS:  Failed to begin\r\n");
                return APP_CODE_ERROR_FAILED_TO_BEGIN_DNS_RESOLUTION;
            }

            while((dnsResult = TCPIP_DNS_IsResolved((const char *)appData.host_1, &appData.host_ipv4, IP_ADDRESS_TYPE_IPV4)) == TCPIP_DNS_RES_PENDING)
            {
                if(APP_TIMER_Expired_ms(&timeout, 10000))
                {
                    return APP_CODE_ERROR_CMD_TIMEOUT;
                }
            }
            if(dnsResult != (TCPIP_DNS_RES_OK))
            {
                SYS_CONSOLE_PRINT("App:  DNS:  Resolution failed - Aborting\r\n");
                return APP_CODE_ERROR_DNS_FAILED;
            } 
            else if(dnsResult == TCPIP_DNS_RES_OK)
            {
               SYS_CONSOLE_PRINT("App:  DNS:  Resolved IPv4 Address: %d.%d.%d.%d for host '%s'\r\n", 
                    appData.host_ipv4.v4Add.v[0],appData.host_ipv4.v4Add.v[1],appData.host_ipv4.v4Add.v[2],
                    appData.host_ipv4.v4Add.v[3],appData.host_1);
            }  
            SYS_CONSOLE_PRINT("App:  TCPIP:  Opening socket to '%d.%d.%d.%d:%d'\r\n", 
                appData.host_ipv4.v4Add.v[0], appData.host_ipv4.v4Add.v[1], appData.host_ipv4.v4Add.v[2],
                appData.host_ipv4.v4Add.v[3], appData.port_1);
            
            appData.socket_1 = TCPIP_TCP_ClientOpen(IP_ADDRESS_TYPE_IPV4,
                                                         appData.port_1,
                                                          (IP_MULTI_ADDRESS*) &send_addr );   // &send_addr
            if (appData.socket_1 == INVALID_SOCKET)
            {
                SYS_CONSOLE_MESSAGE("Could not start connection\r\n");
            }
            SYS_CONSOLE_MESSAGE("Starting connection\r\n");
            appData.state = APP_AUTH_SERVICE;
        }
        break;

        case APP_AUTH_SERVICE:
        {
            memset(buffer2, 0, sizeof(buffer2));
            if (!TCPIP_TCP_IsConnected(appData.socket_1))
            {
                break;
            }
            if(TCPIP_TCP_PutIsReady(appData.socket_1) == 0)
            {
                break;
            }                                              
            nNets = TCPIP_STACK_NumberOfNetworksGet();
            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                MacAddr = TCPIP_STACK_NetAddressMac(netH);
            }
            for (i=0; i<6; i++)
            {
                nullMacAddr[i] = *MacAddr;
                MacAddr++;
            }

            snprintf(appData.MacAddress, sizeof(appData.MacAddress), "%02x:%02x:%02x:%02x:%02x:%02x", nullMacAddr[0],
                    nullMacAddr[1],nullMacAddr[2],nullMacAddr[3],nullMacAddr[4],nullMacAddr[5]);
            
            appData.path="/v1/iot_service/devices/login";
            
            sprintf(buffer2, "POST %s HTTP/1.1\r\n"
                    "Host: %s:80\r\n"
                    "Content-Type:application/json\r\n"            
                    "Content-Length:29\r\n"
                    "api-token:%s\r\n"
                    "\r\n"
                     "{\"macId\":\"%s\"}", appData.path, appData.host_1, appData.ProToken, appData.MacAddress);
                                
            uint16_t check = TCPIP_TCP_ArrayPut(appData.socket_1, (uint8_t*)buffer2, strlen(buffer2));
      
            if(check != 0 && check >= strlen(buffer2)){
                
                appData.state = APP_AUTH_RESPONSE;
            }else{
                appData.state = APP_AUTH_SERVICE;
            }       
            break;
        }
        
        case APP_AUTH_RESPONSE:
        {
            memset(buffer3, 0, sizeof(buffer3));

            if (!TCPIP_TCP_IsConnected(appData.socket_1))
            {
                break;
            }
            if (TCPIP_TCP_GetIsReady(appData.socket_1))
            {
                TCPIP_TCP_ArrayGet(appData.socket_1, (uint8_t*)buffer3, sizeof(buffer3));
                if(buffer3[156] == '\0')
                {
                    appData.state = APP_AUTH_RESPONSE;
                }
                else{
                    //SYS_TMR_CallbackStop(appData.timerHandle);
                    appData.state = APP_CHECK_AUTH_RESPONSE;
                }
            }
            break;
        }

        case APP_CHECK_AUTH_RESPONSE:
        {
            char *responseCode = strstr((char*)buffer3, "200 OK");
            if (responseCode != NULL){
                appData.state = APP_PARSE_TOKEN;
            }
            else{
                appData.state = APP_AUTH_SERVICE;
//                appData.timerHandle = SYS_TMR_CallbackPeriodic(10000, 1, tmrcallback);
            }
            break;
        }

        case APP_PARSE_TOKEN:
        {
            char *data_pointer;
            data_pointer = strchr(buffer3, '{');
            JSON_Value *root_value = json_parse_string(data_pointer);
            if(json_value_get_type(root_value) != JSONObject)
                //appData.state = APP_AUTH_SERVICE; //added to call for authToken once again  
                return -1;
            JSON_Object * tObject = json_value_get_object(root_value);
            appData.authToken1 = json_object_dotget_string(tObject, "authToken");
            strcpy(appData.authToken ,appData.authToken1);
            if(appData.authToken != NULL){
                BSP_LEDOff(BSP_LED_1_CHANNEL, BSP_LED_1_PORT);
                BSP_LEDOff(BSP_LED_2_CHANNEL, BSP_LED_2_PORT);
                BSP_LEDOff(BSP_LED_3_CHANNEL, BSP_LED_3_PORT);
                BSP_LEDOn(BSP_LED_4_CHANNEL, BSP_LED_4_PORT);
                appData.state = APP_REGISTER_USER;
            }
            break;
        }

        case APP_REGISTER_USER:
        {

            memset(buffer2, 0, sizeof(buffer2));
            if (!TCPIP_TCP_IsConnected(appData.socket_1))
            {
                break;
            }
            if(TCPIP_TCP_PutIsReady(appData.socket_1) == 0)
            {
                break;
            }                                              

            snprintf(appData.MacAddress, sizeof(appData.MacAddress), "%02x:%02x:%02x:%02x:%02x:%02x", nullMacAddr[0],
                    nullMacAddr[1],nullMacAddr[2],nullMacAddr[3],nullMacAddr[4],nullMacAddr[5]);
            
            appData.path="/v1/iot_service/devices";
            
            sprintf(buffer2, "PUT %s HTTP/1.1\r\n"
                    "Host: %s:80\r\n"
                    "Content-Type:application/json\r\n"            
                    "Content-Length:29\r\n"
                    "provision-token:%s\r\n"
                    "Cookie: auth-token=%s\r\n"
                    "\r\n"
                     "{\"macId\":\"%s\"}", appData.path, appData.host_1, appData.ProToken, appData.authToken, appData.MacAddress);
            
            
            uint16_t check = TCPIP_TCP_ArrayPut(appData.socket_1, (uint8_t*)buffer2, strlen(buffer2));
            if(check != 0 && check >= strlen(buffer2)){
                appData.state = APP_REGISTRATION_RESPONSE;
            }
            break;
        }

        case APP_REGISTRATION_RESPONSE:
        { 
            memset(buffer3, 0, sizeof(buffer3));
            if (!TCPIP_TCP_IsConnected(appData.socket_1))
            {
                break;
            }
          
            if (TCPIP_TCP_GetIsReady(appData.socket_1))
            {
                                      
                TCPIP_TCP_ArrayGet(appData.socket_1, (uint8_t*)buffer3, sizeof(buffer3));
                if(buffer3[156] == '\0')
                {
                    appData.state = APP_REGISTRATION_RESPONSE;
                }
                else{
                    appData.state = APP_CHECK_RESPONSE;
                }
            }
            break;
        }

        case APP_CHECK_RESPONSE:
        {
            char *responseCode = strstr((char*)buffer3, "200 OK");
            if (responseCode != NULL){
                appData.state = APP_PARSE_SCOPEID;
            }
            else{
                appData.state = APP_AUTH_SERVICE;
            }
            break;
        }

        case APP_PARSE_SCOPEID:
        {
            char *data_pointer;
            data_pointer = strchr(buffer3, '{');
            JSON_Value *root_value = json_parse_string(data_pointer);
            if(json_value_get_type(root_value) != JSONObject)
                return -1;
            JSON_Object * tObject = json_value_get_object(root_value);
            appData.scopeid1 = json_object_dotget_string(tObject, "scopeId");
            strcpy(appData.scopeid ,appData.scopeid1);
            appData.scopeid[25] = '\0';
            appData.state = Get_id;
            break;
        }
        case Get_id:
        {
            
            BME280_GetID();
            appData.state = WEATHER_CALIBRATION;
            break;
        }
        
        case WEATHER_CALIBRATION:
        {
            if(RxBuffer[0] == 0x60){
                weather1();
                appData.state = WEATHER_CALIBRATION1;
            }
            //else
            //{
            //    appData.state = Get_id;
            //}
            break;
        }
        
        case WEATHER_CALIBRATION1:
        {
            if(RxBuffer[3] > 0){
                calculate_BME280_ReadCalibrationParams1();
                RxBuffer[1] = 23;
                weather2();
                appData.state = WAIT_FOR_CALIBRATION_VALUES;
            }
            break;
        }
        
        case WAIT_FOR_CALIBRATION_VALUES:
        {
            if(RxBuffer[1] != 23){
                calculate_BME280_ReadCalibrationParams2();
                set_sleep_mode();
                RxBuffer[0] = 23;
                //appData.timerHandle = SYS_TMR_CallbackPeriodic(5000, 1, tmrcallback);
                appData.state = set_config;
            }
            
            break;
        }
        
        case set_config:
        {
            if(RxBuffer[0] != 23){
                set_the_config();
                RxBuffer[0] = 23;
                BME280_GetConfig();
                select_state = SamplingTemperature;
                appData.state = APP_STATE_IDLE;
                //appData.timerHandle = SYS_TMR_CallbackPeriodic(5000, 1, tmrcallback);

            }
            break;
        }
                 
       
        case APP_STATE_IDLE:
            break;
        
        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }        
    }
}

void APP_TEMP_Tasks ( void )
{   
    static int validConfig = 0;
    /* Check the application's current state. */
    switch ( select_state )
    {
        /* Application's initial state. */
         
        case SamplingTemperature:
        {
            
            if(RxBuffer[0] != 23){
                value = 0;
                RxBuffer[0] = 23;
                BME280_GetCtrlMeasurement();
                appData.state = set_config;
                select_state = Set_Mode1;
            }
            break;
        
        }
        
        case Set_Mode1:
        {
            if(RxBuffer[0] != 23){
                chip_id[0] = RxBuffer[0];
                RxBuffer[0] = 23;   // this should be after setoversamplingTemperature()
                //chip_id[0] = 27;           // temperature sample is 1 and pressure sampling is 8, mode set to normal
                setoversamplingTemperature();
                
                BME280_GetCtrlMeasurement();
                select_state = Set_Mode2;
            }
            break;
        }
        
        case Set_Mode2:
        {
            if(RxBuffer[0] != 23){
                chip_id[0] = RxBuffer[0];
                set_normal_mode();
                value = 0;
                RxBuffer[0] = 23;
                //set_mode();
                BME280_GetStatus();               //BME280_GetCtrlMeasurement(); 
                //appData.state = Set_Mode_Successfull;
                select_state = Read_Measurements1;
            }
            break;
        }
         
        case Read_Measurements1:
        {
            if(RxBuffer[0] != 23){
                bme_status = BME280_IsMeasuring();
                if( bme_status > 0){
                    delay();
                    value = 0;
                    RxBuffer[0] = 23;
                    BME280_GetStatus();
                    break;
                }
                tmp_data[2] = 23;
                BME280_ReadMeasurements();
                
                select_state = Reading_values1;
            }
            break;
        }
        
        case Reading_values1:
        {
            if(tmp_data[2] != 23){
                
                adc_t1 = (uint32_t)tmp_data[0];
                adc_t1 = adc_t1 << 12 & 0x000FFFFF ;
                adc_t = adc_t1 | ((((uint32_t)tmp_data[1] << 4) | (((uint32_t)tmp_data[2] >> 4))) & 0x00000FFF) ;

                select_state = READ_Temperature;
            }
            break;
        }
        
        case READ_Temperature:
        {
            //sysObj.drvI2C0 = DRV_I2C_Initialize(DRV_I2C_INDEX_0, (SYS_MODULE_INIT *)&drvI2C0InitData);
            tmp_f = BME280_GetTemperature();
            f_temp = (tmp_f / 100);
            RxBuffer[0] = 0x00;
            BME280_SoftReset();
            select_state = TMP_SEND;//SamplingTemperature;//select_state = TMP_SEND;        //this has to be changed to continue the regular flow
            tmp_data[2] = 0;

            break;
        }

        
        case TMP_SEND: 
        {
            memset(buffer2, 0, sizeof(buffer2));
#if 1
            if (!TCPIP_TCP_IsConnected(appData.socket_1))
            {
                break;
            }
            if(TCPIP_TCP_PutIsReady(appData.socket_1) == 0)
            {
                break;
            }                                              
            nNets = TCPIP_STACK_NumberOfNetworksGet();
            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                MacAddr = TCPIP_STACK_NetAddressMac(netH);
            }
            for (i=0; i<6; i++)
            {
                nullMacAddr[i] = *MacAddr;
                MacAddr++;
            }
#endif
            snprintf(appData.MacAddress, sizeof(appData.MacAddress), "%02x:%02x:%02x:%02x:%02x:%02x", nullMacAddr[0],
                    nullMacAddr[1],nullMacAddr[2],nullMacAddr[3],nullMacAddr[4],nullMacAddr[5]);

            appData.path="/v1/iot_service/devices/states";
            
            sprintf(buffer2, "POST %s HTTP/1.1\r\n"
                    "Host: %s:80\r\n"
                    "Content-Type:application/json\r\n"            
                    "Content-Length:82\r\n"
                    "Cookie: auth-token=%s\r\n"
                    "\r\n"
                    "{\"macId\":\"%s\",\"states\":[{\"temperature\":%d,\"time\":\"%llu\"}]}"
                    , appData.path, appData.host_1, appData.authToken, appData.MacAddress, f_temp, msec);

            uint16_t check1 = TCPIP_TCP_ArrayPut(appData.socket_1, (uint8_t*)buffer2, strlen(buffer2));
      
            if(check1 != 0 && check1 >= strlen(buffer2)){
                select_state = APP_AUTH_RESPONSE1;
            }else{
                select_state = TMP_SEND;
            }     
            break;
        }

        case APP_AUTH_RESPONSE1:
        {
            memset(buffer3, 0, sizeof(buffer3));

            if (!TCPIP_TCP_IsConnected(appData.socket_1))
            {
                break;
            }
            if (TCPIP_TCP_GetIsReady(appData.socket_1))
            {
                TCPIP_TCP_ArrayGet(appData.socket_1, (uint8_t*)buffer3, sizeof(buffer3));
                if(buffer3[156] == '\0')
                {
                    select_state = APP_AUTH_RESPONSE1;
                }
                else{
                    //SYS_TMR_CallbackStop(appData.timerHandle);
                    select_state = APP_CHECK_AUTH_RESPONSE1;
                }
            }
            break;
        }
        
        case APP_CHECK_AUTH_RESPONSE1:
        {
            char *responseCode = strstr((char*)buffer3, "200 OK");
            if (responseCode != NULL){
                RxBuffer[0] = 0x00;
                select_state = SamplingTemperature;

            }
            else{
                select_state= TMP_SEND;
            }
            break;
        }
        case APP_STATE_IDLE:
            break;
        
        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }       
        
   }
}



